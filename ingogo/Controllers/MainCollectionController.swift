//
//  ViewController.swift
//  ingogo
//
//  Created by Navid Farshchi on 18/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit

class MainCollectionController: UICollectionViewController, UICollectionViewDelegateFlowLayout, DataManagerDelegate {
    
    private var activityIndicator : UIActivityIndicatorView!
    
    var detailViewController = DetailViewController()
    let cellStyle1ID = "PatternCollectionViewCellStyle1ID"
    let cellStyle2ID = "PatternCollectionViewCellStyle2ID"
    let cellStyle3ID = "PatternCollectionViewCellStyle3ID"

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        collectionView?.register(PatternCollectionViewCellStyleLeft.self, forCellWithReuseIdentifier: cellStyle1ID)
        collectionView?.register(PatternCollectionViewCellStyleCenter.self, forCellWithReuseIdentifier: cellStyle2ID)
        collectionView?.register(PatternCollectionViewCellStyleRight.self, forCellWithReuseIdentifier: cellStyle3ID)
        collectionView?.alwaysBounceVertical = true
        self.navigationItem.title = "InGoGo"
        addActivityIndicator()
        
        DataManager.shared.registerDataDelegate(self)
        DataManager.shared.initialize()
        
    }
    
    func addActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.activityIndicatorViewStyle = .gray
        let barButton = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.setRightBarButton(barButton, animated: true)
    }
    
    //MARK: - UICollectionView Delegates
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.shared.dataArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = PatternCollectionViewCell()
        let cellStyle = indexPath.row % 3
        switch cellStyle {
        case 0:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellStyle1ID, for: indexPath) as! PatternCollectionViewCellStyleLeft
        case 1:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellStyle2ID, for: indexPath) as! PatternCollectionViewCellStyleCenter
        case 2:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellStyle3ID, for: indexPath) as! PatternCollectionViewCellStyleRight
        default:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellStyle1ID, for: indexPath) as! PatternCollectionViewCellStyleLeft
        }
            
        cell.fillWithData(DataManager.shared.dataArray[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellStyle = indexPath.row % 3
        var height : CGFloat = 0
        switch cellStyle {
        case 0:
            height = Constants.sizes.ROW_HEIGHT_STYLE_1
        case 1:
            height = Constants.sizes.ROW_HEIGHT_STYLE_2
        case 2:
            height = Constants.sizes.ROW_HEIGHT_STYLE_3
        default:
            height = Constants.sizes.ROW_HEIGHT_STYLE_1
        }
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (DataManager.shared.dataArray[indexPath.item].numComments > 0) {
            //Show detail view controller
            print("tapped")
            detailViewController.setData(DataManager.shared.dataArray[indexPath.item])
            detailViewController.modalTransitionStyle = .flipHorizontal
            present(detailViewController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - DataManager Delegates
    func onDataReady() {
        DispatchQueue.main.async {
            let contentOffset = self.collectionView?.contentOffset
            self.collectionView?.reloadData()
            self.collectionView?.setContentOffset(contentOffset!, animated: true)
        }
    }
    
    func onDataReadingInProgress() {
        activityIndicator.startAnimating()
    }
    
    func onDataReadingFinished() {
        activityIndicator.stopAnimating()
    }
    
    func showErrorMessage(message : String) {
        
    }

}

