//
//  DetailViewController.swift
//  ingogo
//
//  Created by Navid Farshchi on 20/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {
    
    var pattern = Pattern()
    
    var titleLabel = UILabel()
    var dateLabel = UILabel()
    var patternImageView = UIImageView()
    var numberOfLoveLabel = UILabel()
    var numberOfLoveTitleLabel = UILabel()
    var numberOfCommentLabel = UILabel()
    var numberOfCommentTitleLabel = UILabel()
    var numberOfVotesLabel = UILabel()
    var numberOfVotesTitleLabel = UILabel()
    var userNameLabel = UILabel()
    var colorViewsArray : [UIView] = []
    var numberStackView = UIStackView()
    var colorsStackView = UIStackView()
    var descriptionTextView = UITextView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createUIElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fillWithData(self.pattern)
    }
    
    func setData(_ data : Pattern) {
        self.pattern = data
    }
    
    func createUIElements() {
        //Creat close button
        let closeButton = UIButton(type: .system)
        closeButton.setTitle("Back", for: .normal)
        closeButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.backgroundColor = Constants.Colors.cellBackgroundColor
        self.view.addSubview(closeButton)
        
        closeButton.anchor(top: self.view.topAnchor, left: nil, bottom: nil, right: self.view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 70, heightConstant: 40)
        
        numberStackView = UIStackView(arrangedSubviews: [numberOfLoveLabel, numberOfLoveTitleLabel, numberOfVotesLabel, numberOfVotesTitleLabel, numberOfCommentLabel, numberOfCommentTitleLabel])
        
        titleLabel.applyTitleStyle()
        dateLabel.applyDateStyle()
        numberOfLoveLabel.applyNumberStyle()
        numberOfVotesLabel.applyNumberStyle()
        numberOfCommentLabel.applyNumberStyle()
        numberOfLoveTitleLabel.applyNumberTitleStyle()
        numberOfVotesTitleLabel.applyNumberTitleStyle()
        numberOfCommentTitleLabel.applyNumberTitleStyle()
        userNameLabel.applyUserNameStyle()
        numberStackView.applyVerticalStackViewStyle()
        descriptionTextView.applyDescriptionTextViewStyle()
        colorsStackView.applyHorizontalStackViewStyle()
        
        self.view.addSubview(titleLabel)
        self.view.addSubview(dateLabel)
        self.view.addSubview(patternImageView)
        self.view.addSubview(numberStackView)
        self.view.addSubview(colorsStackView)
        self.view.addSubview(userNameLabel)
        self.view.addSubview(descriptionTextView)
        
        
        numberStackView.applyHorizontalStackViewStyle()
        
        titleLabel.anchor(top: closeButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 30)
        titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        userNameLabel.anchor(top: titleLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 15)
        userNameLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        dateLabel.anchor(top: userNameLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 15)
        dateLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        patternImageView.anchor(top: dateLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)
        
        numberStackView.anchor(top: patternImageView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 5, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 30)
        
        colorsStackView.anchor(top: numberStackView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 25)
        
        descriptionTextView.anchor(top: colorsStackView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 150)
        
    }
    
    func fillWithData(_ data : Pattern) {
        
        titleLabel.text = data.title
        dateLabel.text = data.dateCreated
        
        numberOfCommentTitleLabel.text = Constants.strings.COMMENTS_TITLE
        numberOfVotesTitleLabel.text = Constants.strings.VOTES_TITLE
        numberOfLoveTitleLabel.text = Constants.strings.LOVES_TITLE
        
        numberOfLoveLabel.text = "\(data.numHearts)"
        numberOfVotesLabel.text = "\(data.numVotes)"
        numberOfCommentLabel.text = "\(data.numComments)"
        
        userNameLabel.text = "By: \(data.userName)"
        
        //Load image Async and save data for later smooth loading
        patternImageView.applyPatternImageViewStyle()
        DispatchQueue.main.async {
            if (data.imageData != nil) {
                self.patternImageView.image = UIImage.init(data: data.imageData!)
            } else {
                self.patternImageView.image = UIImage(named: "placeholder")
                
                URLSession.shared.dataTask( with: NSURL(string:data.imageUrl)! as URL, completionHandler: {
                    (imageData, response, error) -> Void in
                    DispatchQueue.main.async {
                        if let imgdata = imageData {
                            self.patternImageView.image = UIImage(data: imgdata)
                            data.imageData = imgdata
                        }
                    }
                }).resume()
            }
        }
        
        
        //Clear colorStackView
        DispatchQueue.main.async {
            for view in self.colorsStackView.subviews {
                view.removeFromSuperview()
            }
            //Add some view with background colors to colorStackView
            for i in 0..<data.colors.count {
                let tempView = UILabel()
                tempView.text = " "
                tempView.backgroundColor = hexStringToUIColor(hex: data.colors[i])
                tempView.layer.cornerRadius = 5
                tempView.clipsToBounds = true
                self.colorsStackView.addArrangedSubview(tempView)
            }
        }
        
        //Load Descryption in textView with attibutedText and cache for later use
        if (data.attributedDescription == nil) {
            DispatchQueue.global(qos: .background).async {
                let htmlData = NSString(string: data.description).data(using: String.Encoding.unicode.rawValue)
                let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
                let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
                
                data.attributedDescription = attributedString
                DispatchQueue.main.async {
                    self.descriptionTextView.attributedText = attributedString
                }
            }
        } else {
            DispatchQueue.main.async {
                self.descriptionTextView.attributedText = data.attributedDescription
            }
        }
        
    }
    
    @objc func closeAction() {
        dismiss(animated: true, completion: nil)
    }
    
}
