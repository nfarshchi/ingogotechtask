//
//  PatternTableViewCell.swift
//  ingogo
//
//  Created by Navid Farshchi on 19/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit

class PatternCollectionViewCellStyleLeft: PatternCollectionViewCell {
    
    override func creatUI() {
        super.creatUI()
        addStyle1Constraints()
    }
    
}
