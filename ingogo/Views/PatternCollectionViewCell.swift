//
//  PatternTableViewCell.swift
//  ingogo
//
//  Created by Navid Farshchi on 19/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit

class PatternCollectionViewCell: UICollectionViewCell {

    var titleLabel = UILabel()
    var dateLabel = UILabel()
    var patternImageView = UIImageView()
    var numberOfLoveLabel = UILabel()
    var numberOfLoveTitleLabel = UILabel()
    var numberOfCommentLabel = UILabel()
    var numberOfCommentTitleLabel = UILabel()
    var numberOfVotesLabel = UILabel()
    var numberOfVotesTitleLabel = UILabel()
    var userNameLabel = UILabel()
    var colorViewsArray : [UIView] = []
    var numberStackView = UIStackView()
    var colorsStackView = UIStackView()
    var descriptionTextView = UITextView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        creatUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fillWithData(_ data : Pattern) {

        titleLabel.text = data.title
        dateLabel.text = data.dateCreated
        
        numberOfCommentTitleLabel.text = Constants.strings.COMMENTS_TITLE
        numberOfVotesTitleLabel.text = Constants.strings.VOTES_TITLE
        numberOfLoveTitleLabel.text = Constants.strings.LOVES_TITLE
        
        numberOfLoveLabel.text = "\(data.numHearts)"
        numberOfVotesLabel.text = "\(data.numVotes)"
        numberOfCommentLabel.text = "\(data.numComments)"
        
        userNameLabel.text = "By: \(data.userName)"
        
        
        
        //Load image Async and save data for later smooth loading
        patternImageView.applyPatternImageViewStyle()
        
        if (data.imageData != nil) {
            patternImageView.image = UIImage.init(data: data.imageData!)
        } else {
            patternImageView.image = UIImage(named: "placeholder")
            
            URLSession.shared.dataTask( with: NSURL(string:data.imageUrl)! as URL, completionHandler: {
                (imageData, response, error) -> Void in
                DispatchQueue.main.async {
                    if let imgdata = imageData {
                        self.patternImageView.image = UIImage(data: imgdata)
                        data.imageData = imgdata
                    }
                }
            }).resume()
        }
        
        //Clear colorStackView
        for view in colorsStackView.subviews {
            view.removeFromSuperview()
        }
        //Add some view with background colors to colorStackView
        for i in 0..<data.colors.count {
            let tempView = UILabel()
            tempView.text = " "
            tempView.backgroundColor = hexStringToUIColor(hex: data.colors[i])
            tempView.layer.cornerRadius = 5
            tempView.clipsToBounds = true
            colorsStackView.addArrangedSubview(tempView)
        }
        
        //Load Descryption in textView with attibutedText and cache for later use
        if (data.attributedDescription == nil) {
            DispatchQueue.global(qos: .background).async {
                let htmlData = NSString(string: data.description).data(using: String.Encoding.unicode.rawValue)
                let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
                let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
                
                data.attributedDescription = attributedString
                DispatchQueue.main.async {
                    self.descriptionTextView.attributedText = attributedString
                }
            }
        } else {
            self.descriptionTextView.attributedText = data.attributedDescription
        }
    }
    
    func creatUI() {
        backgroundColor = Constants.Colors.cellBackgroundColor

        numberStackView = UIStackView(arrangedSubviews: [numberOfLoveLabel, numberOfLoveTitleLabel, numberOfVotesLabel, numberOfVotesTitleLabel, numberOfCommentLabel, numberOfCommentTitleLabel])
        
        titleLabel.applyTitleStyle()
        dateLabel.applyDateStyle()
        numberOfLoveLabel.applyNumberStyle()
        numberOfVotesLabel.applyNumberStyle()
        numberOfCommentLabel.applyNumberStyle()
        numberOfLoveTitleLabel.applyNumberTitleStyle()
        numberOfVotesTitleLabel.applyNumberTitleStyle()
        numberOfCommentTitleLabel.applyNumberTitleStyle()
        userNameLabel.applyUserNameStyle()
        numberStackView.applyVerticalStackViewStyle()
        descriptionTextView.applyDescriptionTextViewStyle()
        colorsStackView.applyHorizontalStackViewStyle()
        
        
        
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(dateLabel)
        self.contentView.addSubview(patternImageView)
        self.contentView.addSubview(numberStackView)
        self.contentView.addSubview(colorsStackView)
        self.contentView.addSubview(userNameLabel)
        self.contentView.addSubview(descriptionTextView)
        
    }

    func addStyle1Constraints() {
        
        titleLabel.anchor(top: self.contentView.topAnchor, left: self.contentView.leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 30)
        
        dateLabel.anchor(top: self.contentView.topAnchor, left: nil, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 30)
        
        patternImageView.anchor(top: titleLabel.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: self.contentView.frame.width / 2, heightConstant: 150)
        
        numberStackView.anchor(top: patternImageView.topAnchor, left: patternImageView.rightAnchor, bottom: patternImageView.bottomAnchor, right: self.contentView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        colorsStackView.anchor(top: patternImageView.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 25)
        
        userNameLabel.anchor(top: colorsStackView.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)
        
        descriptionTextView.anchor(top: userNameLabel.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 150)
    }
    
    func addStyle2Constraints() {
        
        numberStackView.applyHorizontalStackViewStyle()
        
        titleLabel.anchor(top: self.contentView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 30)
        titleLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        
        userNameLabel.anchor(top: titleLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 15)
        userNameLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        
        dateLabel.anchor(top: userNameLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 15)
        dateLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        
        patternImageView.anchor(top: dateLabel.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)
        
        numberStackView.anchor(top: patternImageView.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: self.contentView.rightAnchor, topConstant: 5, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 30)
        
        colorsStackView.anchor(top: numberStackView.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 25)
        
        descriptionTextView.anchor(top: colorsStackView.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 150)
        
    }
    
    func addStyle3Constraints() {
        
        titleLabel.anchor(top: self.contentView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 30)
        titleLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        
        userNameLabel.anchor(top: titleLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 15)
        userNameLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        
        dateLabel.anchor(top: userNameLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 15)
        dateLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        
        
        patternImageView.anchor(top: dateLabel.bottomAnchor, left: nil, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: self.contentView.frame.width / 2, heightConstant: 150)
        
        numberStackView.anchor(top: patternImageView.topAnchor, left: nil, bottom: patternImageView.bottomAnchor, right: patternImageView.leftAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        colorsStackView.anchor(top: patternImageView.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 25)
        
        descriptionTextView.anchor(top: colorsStackView.bottomAnchor, left: self.contentView.leftAnchor, bottom: nil, right: self.contentView.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 150)
    }
}
