//
//  TitleLabel.swift
//  ingogo
//
//  Created by Navid Farshchi on 19/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit


extension UILabel {
    func applyTitleStyle() {
        self.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        self.textAlignment = .left
        self.textColor = UIColor.black
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func applyDateStyle() {
        self.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        self.textAlignment = .right
        self.textColor = UIColor.darkGray
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func applyUserNameStyle() {
        self.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        self.textAlignment = .left
        self.textColor = UIColor.black
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func applyNumberStyle() {
        self.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        self.textAlignment = .left
        self.textColor = UIColor.black
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func applyNumberTitleStyle() {
        self.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        self.textAlignment = .left
        self.textColor = UIColor.black
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}

extension UITextView {
    func applyDescriptionTextViewStyle() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .white
        self.isScrollEnabled = false
        self.isEditable = false
    }
}

extension UIStackView {
    func applyHorizontalStackViewStyle() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.axis = .horizontal
        self.distribution = .fillEqually
        self.alignment = .fill
        self.spacing = 2
    }
    
    func applyVerticalStackViewStyle() {
        self.axis = .vertical
        self.distribution = .equalSpacing
        self.alignment = .fill
        self.spacing = 5
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}

func hexStringToUIColor (hex:String) -> UIColor {
    let cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) {
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: topConstant).isActive = true
        }
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: leftConstant).isActive = true
        }
        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant).isActive = true
        }
        if let right = right {
            self.rightAnchor.constraint(equalTo: right, constant: -rightConstant).isActive = true
        }
        if heightConstant > 0 {
            self.heightAnchor.constraint(equalToConstant: heightConstant).isActive = true
        }
        if widthConstant > 0 {
            self.widthAnchor.constraint(equalToConstant: widthConstant).isActive = true
        }
    }
}

extension UIImageView {
    func applyPatternImageViewStyle() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.contentMode = .scaleAspectFit
    }
    
    
}
