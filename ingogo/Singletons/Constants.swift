//
//  Constants.swift
//  ingogo
//
//  Created by Navid Farshchi on 18/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let SERVER = "http://www.colourlovers.com/"
    static let REQUEST = "api/patterns/random"
    static let WEBSOCKET = URL(string: "ws://demos.kaazing.com/echo")
    
    struct sizes {
        static let LEFT_MARGIN : CGFloat = 10
        static let RIGHT_MARGIN : CGFloat = 10
        static let TOP_MARGIN : CGFloat = 10
        static let ROW_HEIGHT_STYLE_1 : CGFloat = 420
        static let ROW_HEIGHT_STYLE_2 : CGFloat = 476
        static let ROW_HEIGHT_STYLE_3 : CGFloat = 443
        
    }
    
    struct strings {
        static let COMMENTS_TITLE = "Comments"
        static let VOTES_TITLE = "Votes"
        static let LOVES_TITLE = "Loves"
    }
    
    struct Colors {
        static let cellBackgroundColor = UIColor(red: (238.0/255.0), green: (238.0/255.0), blue: (238.0/255.0), alpha: 1.0)
    }
}
