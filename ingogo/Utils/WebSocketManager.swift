//
//  WebSocketManager.swift
//  ingogo
//
//  Created by Navid Farshchi on 19/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import Starscream

class WebSocketManager: WebSocketDelegate {
    
    static let shared = WebSocketManager()
    let socket = WebSocket(url: Constants.WEBSOCKET!)
    var delegate : WSDelegate!
    var dataToWrite : [Data] = []
    
    func initialize() {
        socket.delegate = self
        socket.connect()
    }
    
    func registerDelegate(_ delegate : WSDelegate) {
        self.delegate = delegate
    }
    
    //MARK: - Write on Socket
    func writeDataOnSocket(_ data : Data) {
        dataToWrite.append(data)
        let rnd = randomBetween(lowerBound: 1, upperBound: 3)
        if (socket.isConnected && rnd == 2) {
            while dataToWrite.count > 0 {
                socket.write(data: dataToWrite[0])
                dataToWrite.removeFirst()
            }
        } else {
            socket.connect()
        }
    }
    
    //MARK: - StarScream WebSocket delegates methods
    func websocketDidConnect(socket: WebSocketClient) {
        print("didConnect")
        //After connect check to see if there is data to write waiting or not, if so write those data on socket
        while dataToWrite.count > 0 {
            socket.write(data: dataToWrite[0])
            dataToWrite.removeFirst()
        }
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("didDisConnect: \(error?.localizedDescription ?? "No error")")
        //Try to reconnect
        socket.connect()
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("didReceiveMessage: \(text)")
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("didReceiveData : \(data)" )
        delegate.onDataFromWebSocket(data)
    }
}

protocol WSDelegate {
    func onDataFromWebSocket(_ data : Data)
}
