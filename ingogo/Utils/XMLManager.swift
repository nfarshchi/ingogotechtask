//
//  XMLParser.swift
//  ingogo
//
//  Created by Navid Farshchi on 19/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation

class XMLManager : NSObject, XMLParserDelegate {
    
    static let shared = XMLManager()
    
    var parser = XMLParser()
    var patternObject = Pattern()
    var delegate : XMLToObjectDelegate!
    
    var elementName = ""
    var isInsideTemplate = false
    var isInsideAuthor = false
    
    func registerDelegate(_ delegate : XMLToObjectDelegate) {
        self.delegate = delegate
    }
    
    func parseXML(xmlData : Data) {
        DispatchQueue.main.async {
            self.parser = XMLParser(data: xmlData)
            self.parser.delegate = self
            
            let succes : Bool = self.parser.parse()
            if succes {
                print ("XML parsed correctly")
            } else {
                print("error in parsing XML")
            }
        }
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {

        self.elementName = elementName
        
        if (elementName == "pattern") {
            //Create new Pattern object
            patternObject = Pattern()
        }
        
        if (elementName == "template") {
            isInsideTemplate = true
        } else if (elementName == "author") {
            isInsideAuthor = true
        }
        
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName == "template") {
            isInsideTemplate = false
        } else if (elementName == "author") {
            isInsideAuthor = false
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if (!data.isEmpty) {
            switch elementName {
            case "title":
                if isInsideTemplate {
                    patternObject.templete.title = data
                } else {
                    patternObject.title = data
                }
            case "id":
                patternObject.id = Int(data)!
            case "userName":
                if (isInsideAuthor) {
                    patternObject.templete.author.userName = data
                } else {
                    patternObject.userName = data
                }
            case "numViews":
                patternObject.numViews = Int(data)!
            case "numVotes":
                patternObject.numVotes = Int(data)!
            case "numComments":
                patternObject.numComments = Int(data)!
            case "numHearts":
                patternObject.numHearts = Int(data)!
            case "rank":
                patternObject.rank = Int(data)!
            case "dateCreated":
                patternObject.dateCreated = data
            case "description":
                patternObject.description = data
            case "url":
                if (isInsideTemplate) {
                    if (isInsideAuthor) {
                        patternObject.templete.author.url = data
                    } else {
                        patternObject.templete.url = data
                    }
                } else {
                    patternObject.url = data
                }
            case "imageUrl":
                patternObject.imageUrl = data
            case "badgeUrl":
                patternObject.badgeUrl = data
            case "apiUrl":
                patternObject.apiUrl = data
            case "hex":
                patternObject.colors.append(data)
            
                
            default:
                print(elementName)
            }
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        delegate.onConvertFromXML(object: patternObject)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("error : " , parseError)
    }
}

protocol XMLToObjectDelegate {
    func onConvertFromXML(object : Pattern)
}
