//
//  Utils.swift
//  ingogo
//
//  Created by Navid Farshchi on 19/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import UIKit

func randomBetween(lowerBound : Int, upperBound : Int) -> Int {
    return Int(arc4random_uniform(UInt32(upperBound - lowerBound)) + UInt32(lowerBound))
}
