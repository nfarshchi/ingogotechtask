//
//  NetworkManager.swift
//  ingogo
//
//  Created by Navid Farshchi on 18/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation

protocol NetworkDelegate {
    func onSuccessWithResponse(resonse : Data)
    func onFailedWithError(error: Error)
    func serviceCallOnProgress()
    func serviceCallFinished()
}

func getRequest(server: String, request: String, delegate : NetworkDelegate) {
    delegate.serviceCallOnProgress()
    
    let urlPath: String = server + request
    let url = URL(string: urlPath)
    
    let task = URLSession.shared.dataTask(with: url! as URL) { data, response, error in
        
        guard error == nil else {
            print("got an error: \(String(describing: error?.localizedDescription))")
            delegate.serviceCallFinished()
            delegate.onFailedWithError(error: error!)
            return
        }
        
        guard let data = data else { return }
        delegate.serviceCallFinished()
        delegate.onSuccessWithResponse(resonse: data)
    }
    
    task.resume()
}

